from django.urls import path
from todos.views import delete_list, edit_list, create_item
from todos.views import todo_list, todoitem, create_view

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todoitem, name="todo_list_detail"),
    path("create/", create_view, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
     path("items/create/", create_item, name="todo_list_create"),
]
