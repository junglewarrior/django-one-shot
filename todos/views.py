from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm

# Create your views here.
def todo_list(request):
    todos_lists = TodoList.objects.all()
    context = {
        "todo_list": todos_lists,
    }
    return render(request, "todos/list.html", context)

def todoitem(request, id):
    items = get_object_or_404(TodoList, id=id)
    context = {
        "todoitem": items,
    }
    return render(request, "todos/detail.html", context)

def create_view(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            items = form.save()
            return redirect("todo_list_detail", id=items.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def edit_list(request, id):
    items = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=items)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=items.id)
    else:
        form = TodoForm(instance=items)
    context = {
        "list_object": items,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def delete_list(request, id):
    items = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        items.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_item(request):
    if request.method == "POST":
        form = TodoForm(request.POST, id=id)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    context = {
        "task_object": todoitem.task,
        "form": form,
    }
    return render(request, "todos/createitem.html")
